# Confer

A language agnostic system for distributing and controlling configuration through the network.

## Features

* [todo] schema validation and definiton
* [todo] proxy agent for controlling caching, authentication and host-level consistency
* [todo] bilateral authentication, and maybe authorisation
* [todo] versioning with rollback
* [todo] configurable cache policies per configuration key
* [todo] client libraries for a thousand billion languages

## Schema definition

### JSON-schema based

Advantages:

* Standardised
* Reuse existing libraries and tooling
* Well-understood format
* May not be suitable for "per-field" properties

Disadvantages:

* Tied to JSON objects
* Software would be constrained to schemas supported by the spec
* Would need to write extensions to the standard should an unsupported feature be desireable, diminishing value of the
  previous advantages
* A bit verbose and reuse doesn't seem straightforward

### Custom DSL

We could define our own schema language and build all validations on top of that. This would be technically more
challenging and add a lot of friction to start using Confer, with the main benefit being flexibility for the system.

A potential issue with JSON-schema is its relative verbosity, which could be mitigated by having a compiler/generator
based on a custom language that generated a JSON-schema definition.

Advantages:

* Schema specification is owned by us and we may add more stuff when necessary without violating any standard
* Validation "scope" doesn't need to be tied to global object
* Language could be tailored to be simpler to write and less verbose
* Technically interesting

Disadvantages:

* Non-standard
* A lot of work for designing and implementing schema mini-language
* No prior documentation or knowledge, requiring a big of documentation effort

## Interacting with the server

### Schema management

Through an authenticated CLI, a user or system should be able to create and update a schema. A schema has to be created
first before being updated, and with each change to its definition a new revision is automatically created. The
schema specification is done by uploading a JSON-schema file to the Confer server from a trusted client (should
authentication be enabled. At least initially, an update replaces the entire definition, and compatible configuration
items remain accessible by applications based on the newer revision.

Basic input:

```bash
confer schema-create --id "hstefan.com/myapp" --src "./myapp-schema-v1.json"
confer schema-update --id "hstefan.com/myapp" --src "./myapp-schema-v2.json"
```

Rough output:

```text
Created schema "hstefan.com/myapp" from file "/home/hstefan/myapp-schema-v1.json"
Revision 0
---
Updated schema "hstefan.com/myapp" from file "/home/hstefan/myapp-schema-v2.json"
Revision 0 -> 1
```

It would also be good to provide ways to migrate existing keys to a representation specified by a new schema, so that
configuration doesn't need to be readed every time it's represented in a different format. Such migration functionality
could also allow users to reflect changes on more recent schema versions to older applications, as long as a two-way
migration is provided by the user.

### Configuration update

TODO
