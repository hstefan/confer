table! {
    user_schemas (id) {
        id -> Integer,
        name -> Varchar,
        version -> Integer,
        specification -> Jsonb,
    }
}
