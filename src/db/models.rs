use serde_json;

#[derive(Queryable)]
pub struct UserSchema {
    pub id: i32,
    pub name: String,
    pub version: i32,
    pub specification: serde_json::Value,
}
