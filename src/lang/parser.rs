use nom::dbg;
use nom::*;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
struct Identifier<'a>(&'a str);

#[derive(Debug, PartialEq)]
struct PathIdentifier<'a>(Vec<Identifier<'a>>);

#[derive(Debug, PartialEq)]
struct EnumDefinition<'a> {
    name: Identifier<'a>,
    values: Vec<Identifier<'a>>,
}

#[derive(Debug, PartialEq)]
struct ConstraintProperty<'a> {
    key: Identifier<'a>,
    value: Literal,
}

#[derive(Debug, PartialEq)]
struct ConstraintDefinition<'a> {
    name: Identifier<'a>,
    constrained_type: BaseType,
    properties: Vec<ConstraintProperty<'a>>,
}

#[derive(Debug, PartialEq)]
struct PropertyType<'a> {
    name: Identifier<'a>,
    type_name: Identifier<'a>,
    nullable: bool,
    constraints: Option<Vec<ConstraintProperty<'a>>>,
}

#[derive(Debug, PartialEq)]
struct TypeDefinition<'a> {
    name: Identifier<'a>,
    properties: Vec<PropertyType<'a>>,
}

#[derive(Debug, PartialEq)]
struct SchemaConstraint<'a> {
    key: Identifier<'a>,
    properties: Vec<PathIdentifier<'a>>,
}

#[derive(Debug, PartialEq)]
struct SchemaDefinition<'a> {
    name: PathIdentifier<'a>,
    properties: Vec<PropertyType<'a>>,
    constraints: Vec<SchemaConstraint<'a>>,
}

#[derive(Debug, PartialEq)]
pub struct FileDescriptor<'a> {
    enums: Vec<EnumDefinition<'a>>,
    constraints: Vec<ConstraintDefinition<'a>>,
    types: Vec<TypeDefinition<'a>>,
    schemas: Vec<SchemaDefinition<'a>>
}

#[derive(Debug, PartialEq)]
enum Literal {
    Str(String),
    Boolean(bool),
    Integer(i64),
    Float(f64),
}

#[derive(Debug, PartialEq)]
enum BaseType {
    Str,
    Boolean,
    Integer,
    Float,
}

#[derive(Debug, PartialEq)]
enum Event<'a> {
    Enum(EnumDefinition<'a>),
    Type(TypeDefinition<'a>),
    Constraint(ConstraintDefinition<'a>),
    Schema(SchemaDefinition<'a>),
}

fn not_identifier_end(c: char) -> bool {
    match c {
        '.' | ',' | ';' | ':' | ' ' | '\n' | '\t' | '\r' | '{' | '}' | '(' | ')' | '[' | ']'
        | '?' => false,
        _ => true,
    }
}

fn parse_number(maybe_num: &str) -> Result<Literal, &str> {
    if let Ok(num) = maybe_num.parse::<i64>() {
        Ok(Literal::Integer(num))
    } else {
        if let Ok(num) = maybe_num.parse::<f64>() {
            Ok(Literal::Float(num))
        } else {
            Err("Unable to parse number literal")
        }
    }
}

named!(
    comment<()>,
    do_parse!(tag!("//") >> take_until_and_consume!("\n") >> ())
);

named!(
    block_comment<()>,
    do_parse!(tag!("/*") >> take_until_and_consume!("*/") >> ())
);

named!(alpha_or_underscore(&str) -> &str, alt!(alpha | tag!("_")));

named!(
    identifier(&str) -> Identifier,
    map!(recognize!(
        tuple!(
            alpha_or_underscore,
            take_while!(not_identifier_end)
        )
    ),
    |x| Identifier(x))
);

named!(
    string_literal(&str) -> Literal,
    map!(
        delimited!(
            tag!("\""),
            escaped_transform!(is_not!("\n\"\\"), '\\', one_of!("\'\"\\\n0123456789abfnrtuvxz")),
            tag!("\"")
        ),
        |x| Literal::Str(x)
    )
);

named!(
    boolean_literal(&str) -> Literal,
    map!(
        alt!(tag!("true") | tag!("false")),
        |x| Literal::Boolean(FromStr::from_str(&x).unwrap())
    )
);

named!(
    signed_digits(&str) -> (Option<&str>, &str),
    pair!(
        opt!(alt!(tag!("+") | tag!("-"))),
        digit
    )
);

named!(maybe_signed_digits(&str) -> &str,
    recognize!(signed_digits)
);

named!(
    number_literal(&str) -> Literal,
    map_res!(
        recognize!(tuple!(
            maybe_signed_digits,
            opt!(pair!(tag!("."), digit)),
            opt!(pair!(
                alt!(tag!("e") | tag!("E")),
                maybe_signed_digits
            ))
        )),
        |x| parse_number(x)
    )
);

named!(
    any_scalar_literal(&str) -> Literal,
    alt!(number_literal | string_literal | boolean_literal)
);

named!(
    array_literal(&str) -> Vec<Literal>,
    delimited!(
        tag!("["),
        separated_list!(
            tag!(","),
            ws!(any_scalar_literal)
        ),
        tag!("]")
    )
);

named!(
    enum_definition(&str) -> EnumDefinition,
    do_parse!(
        tag!("enum") >>
        multispace1 >>
        name: ws!(identifier) >>
        values: delimited!(
            tag!("{"),
            separated_nonempty_list!(
                tag!(","),
                ws!(identifier)
            ),
            tag!("}")
        ) >>
        (EnumDefinition {name, values})
    )
);

named!(
    constraint_property(&str) -> ConstraintProperty,
    do_parse!(
        key: identifier >>
        multispace1 >>
        value: any_scalar_literal >>
        (ConstraintProperty{key, value})
    )
);

named!(
    base_type_identifier(&str) -> BaseType,
    alt!(
        map!(tag!("int"), |_| BaseType::Integer)  |
        map!(tag!("float"), |_| BaseType::Float)  |
        map!(tag!("bool"), |_| BaseType::Boolean) |
        map!(tag!("string"), |_| BaseType::Str)
    )
);

named!(
    constraint_properties(&str) -> Vec<ConstraintProperty>,
    delimited!(
        tag!("{"),
        separated_nonempty_list!(
            tag!(","),
            ws!(constraint_property)
        ),
        tag!("}")
    )
);

named!(
    constraint_definition(&str) -> ConstraintDefinition,
    do_parse!(
        tag!("constraint") >>
        multispace1 >>
        name: ws!(identifier) >>
        tag!("for") >>
        constrained_type: ws!(base_type_identifier) >>
        properties: constraint_properties >>
        (ConstraintDefinition {name, constrained_type, properties})
    )
);

named!(
    type_property(&str) -> PropertyType,
    dbg!(do_parse!(
        name: identifier >>
        ws!(tag!(":")) >>
        type_name: identifier >>
        nullable: map!(opt!(ws!(tag!("?"))), |x| x.is_some()) >>
        constraints: opt!(ws!(constraint_properties)) >>
        (PropertyType{name, type_name, nullable, constraints})
    ))
);

named!(
    type_property_list(&str) -> Vec<PropertyType>,
    separated_list!(tag!(","), ws!(type_property))
);

named!(
    type_definition_body(&str) -> Vec<PropertyType>,
    delimited!(tag!("{"), ws!(type_property_list), tag!("}"))
);

named!(
    type_definition(&str) -> TypeDefinition,
    do_parse!(
        tag!("type") >>
        multispace1 >>
        name: identifier >>
        multispace >>
        properties: type_definition_body >>
        (TypeDefinition{name, properties})
    )
);

named!(
    path_identifier(&str) -> PathIdentifier,
    map!(
        separated_nonempty_list!(tag!("."), identifier),
        PathIdentifier
    )
);

named!(
    path_identifier_list(&str) -> Vec<PathIdentifier>,
    delimited!(
        dbg!(tag!("[")),
        separated_nonempty_list!(tag!(","), ws!(path_identifier)),
        dbg!(tag!("]"))
    )
);

named!(
    schema_constraint(&str) -> SchemaConstraint,
    do_parse!(
        key: ws!(identifier) >>
        properties: dbg!(path_identifier_list) >>
        (SchemaConstraint {key, properties})
    )
);

named!(
    schema_constraint_list(&str) -> Vec<SchemaConstraint>,
    do_parse!(
        tag!("constraints") >>
        multispace >>
        constraints: delimited!(
            tag!("{"),
            separated_list!(tag!(","), dbg!(ws!(schema_constraint))),
            tag!("}")
        ) >>
        (constraints)
    )
);

named!(
    schema_property_list(&str) -> Vec<PropertyType>,
    do_parse!(
        tag!("properties") >>
        multispace >>
        properties: type_definition_body >>
        (properties)
    )
);

named!(
    schema_definition(&str) -> SchemaDefinition,
    do_parse!(
        tag!("schema") >>
        multispace1 >>
        name: ws!(path_identifier) >>
        tag!("{") >>
        properties: ws!(schema_property_list) >>
        constraints: ws!(schema_constraint_list) >>
        tag!("}") >>
        (SchemaDefinition{name, properties, constraints})
    )
);

named!(
    event(&str) -> Event,
    alt!(
        ws!(enum_definition) => { |e| Event::Enum(e) } |
        ws!(type_definition) => { |t| Event::Type(t) } |
        ws!(constraint_definition) => { |c| Event::Constraint(c) } |
        ws!(schema_definition) => { |s| Event::Schema(s) }
    )
);

named!(
    pub file_descriptor(&str) -> FileDescriptor,
    map!(many0!(event), |events: Vec<Event>| {
        let mut fd = FileDescriptor {
            enums: vec![],
            constraints: vec![],
            types: vec![],
            schemas: vec![],
        };
        for e in events {
            match e {
                Event::Enum(e) => fd.enums.push(e),
                Event::Constraint(c) => fd.constraints.push(c),
                Event::Type(t) => fd.types.push(t),
                Event::Schema(s) => fd.schemas.push(s),
            }
        }
        fd
    })
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_identifier() {
        let iden = |x| identifier(x).unwrap().1;
        assert_eq!(iden("foo123,"), Identifier("foo123"));
        assert_eq!(iden("_123foo "), Identifier("_123foo"));
        assert_eq!(iden("b2r\n"), Identifier("b2r"));
        assert_eq!(iden("stef\r"), Identifier("stef"));
        assert_eq!(iden("bunny\t"), Identifier("bunny"));
        assert_eq!(iden("boo{"), Identifier("boo"));
        assert!(identifier("123foo,").is_err());
    }

    #[test]
    fn test_boolean_literal() {
        let bool_lit = |x| boolean_literal(x).unwrap().1;
        assert_eq!(bool_lit("true "), Literal::Boolean(true));
        assert_eq!(bool_lit("false "), Literal::Boolean(false));
        assert!(boolean_literal("garbage").is_err());
    }

    #[test]
    fn test_string_literal() {
        let str_lit = |x| string_literal(x).unwrap().1;
        assert_eq!(str_lit("\"foo\""), Literal::Str("foo".to_string()));
        assert_eq!(
            str_lit("\"s\\\"WHAT\\\"up\""),
            Literal::Str("s\"WHAT\"up".to_string())
        );
    }

    #[test]
    fn test_integer_literal() {
        let num_lit = |x| number_literal(x).unwrap().1;
        assert_eq!(num_lit("4242424242 "), Literal::Integer(4242424242));
        assert_eq!(num_lit("-123456789 "), Literal::Integer(-123456789));
        assert_eq!(num_lit("+987654321 "), Literal::Integer(987654321));
        assert_eq!(num_lit("-3.14 "), Literal::Float(-3.14));
        assert_eq!(num_lit("+6.02E-23 "), Literal::Float(6.02E-23));
        assert!(number_literal("x").is_err());
    }

    #[test]
    fn test_array_literal() {
        let array_lit = |x| array_literal(x).unwrap().1;
        assert_eq!(
            array_lit("[1, -6.03, \"loominardy\", false] "),
            vec![
                Literal::Integer(1),
                Literal::Float(-6.03),
                Literal::Str("loominardy".to_owned()),
                Literal::Boolean(false)
            ]
        );
    }

    #[test]
    fn test_enum_definition() {
        let enum_def = |x| enum_definition(x).unwrap().1;
        assert_eq!(
            enum_def("enum Foo { Bar, Baz }"),
            EnumDefinition {
                name: Identifier("Foo"),
                values: vec![Identifier("Bar"), Identifier("Baz")]
            }
        );

        assert_eq!(
            enum_def("enum\nFoo{A,\tB, C}"),
            EnumDefinition {
                name: Identifier("Foo"),
                values: vec![Identifier("A"), Identifier("B"), Identifier("C")]
            }
        );

        assert!(enum_definition("enumFoo{Bar}").is_err());
        assert!(enum_definition("enum Foo{}").is_err());
    }

    #[test]
    fn test_constraint_definition() {
        let constraint_def = |x| constraint_definition(x).unwrap().1;
        assert_eq!(
            constraint_def(
                "constraint Hostname for string { min_length 10,\nmax_length 100, format\t\"hostname\"\n}"
            ),
            ConstraintDefinition {
                name: Identifier("Hostname"),
                constrained_type: BaseType::Str,
                properties: vec![
                    ConstraintProperty {
                        key: Identifier("min_length"),
                        value: Literal::Integer(10)
                    },
                    ConstraintProperty {
                        key: Identifier("max_length"),
                        value: Literal::Integer(100)
                    },
                    ConstraintProperty {
                        key: Identifier("format"),
                        value: Literal::Str("hostname".to_owned())
                    },
                ]
            }
        );
    }

    #[test]
    fn test_type_definition() {
        let type_def = |x| type_definition(x).unwrap().1;
        assert_eq!(
            type_def(
                "type DatabaseConnection {
                    database: string? {
                        format \"nospaces\"
                    },
                    port: int {
                        min 1024,
                        max 35000
                    },
                    server_address: string { format \"hostname\" }
                }"
            ),
            TypeDefinition {
                name: Identifier("DatabaseConnection"),
                properties: vec![
                    PropertyType {
                        name: Identifier("database"),
                        type_name: Identifier("string"),
                        nullable: true,
                        constraints: Some(vec![
                            ConstraintProperty {
                                key: Identifier("format"),
                                value: Literal::Str("nospaces".to_owned())
                            },
                        ])
                    },
                    PropertyType {
                        name: Identifier("port"),
                        type_name: Identifier("int"),
                        nullable: false,
                        constraints: Some(vec![
                            ConstraintProperty {
                                key: Identifier("min"),
                                value: Literal::Integer(1024)
                            },
                            ConstraintProperty {
                                key: Identifier("max"),
                                value: Literal::Integer(35000)
                            }
                        ])
                    },
                    PropertyType {
                        name: Identifier("server_address"),
                        type_name: Identifier("string"),
                        nullable: false,
                        constraints: Some(vec![ConstraintProperty {
                            key: Identifier("format"),
                            value: Literal::Str("hostname".to_owned())
                        }])
                    },
                ]
            }
        );
    }

    #[test]
    fn test_schema_definition() {
        let schema_def = |x| schema_definition(x).unwrap().1;
        let src = "schema hstefan.Confer {
            properties {
                conferDaemon: ConferDaemon,
                conferProxy: ConferProxy?
            }
            constraints {
                unique [conferDaemon.service, conferProxy.service]
            }
        }";

        let schema = SchemaDefinition {
            name: PathIdentifier(vec![Identifier("hstefan"), Identifier("Confer")]),
            properties: vec![
                PropertyType {
                    name: Identifier("conferDaemon"),
                    type_name: Identifier("ConferDaemon"),
                    nullable: false,
                    constraints: None,
                },
                PropertyType {
                    name: Identifier("conferProxy"),
                    type_name: Identifier("ConferProxy"),
                    nullable: true,
                    constraints: None,
                },
            ],
            constraints: vec![SchemaConstraint {
                key: Identifier("unique"),
                properties: vec![
                    PathIdentifier(vec![Identifier("conferDaemon"), Identifier("service")]),
                    PathIdentifier(vec![Identifier("conferProxy"), Identifier("service")]),
                ],
            }],
        };

        assert_eq!(schema_def(src), schema);
    }

    #[test]
    fn test_schema_property_list() {
        let prop_list = |x| schema_property_list(x).unwrap().1;

        let src = "properties {
            conferDaemon: ConferDaemon,
            conferProxy: ConferProxy?
        }";

        let res = vec![
            PropertyType {
                name: Identifier("conferDaemon"),
                type_name: Identifier("ConferDaemon"),
                nullable: false,
                constraints: None,
            },
            PropertyType {
                name: Identifier("conferProxy"),
                type_name: Identifier("ConferProxy"),
                nullable: true,
                constraints: None,
            },
        ];

        assert_eq!(prop_list(src), res);
    }

    #[test]
    fn test_schema_constraint_list() {
        let constr_list = |x| schema_constraint_list(x).unwrap().1;

        let src = "constraints {
            unique [conferDaemon.service, conferDaemon.database.service]
        }";

        let res = vec![SchemaConstraint {
            key: Identifier("unique"),
            properties: vec![
                PathIdentifier(vec![Identifier("conferDaemon"), Identifier("service")]),
                PathIdentifier(vec![
                    Identifier("conferDaemon"),
                    Identifier("database"),
                    Identifier("service"),
                ]),
            ],
        }];

        assert_eq!(constr_list(src), res);
    }

    #[test]
    fn test_file_descriptior() {
        let src = r#"
constraint RegisteredPort for int {
    minimum 1024, maximum 49151
}

constraint HostPattern for string {
    format "regex"
}

type ServiceAddress {
    hostname: string {
        format "hostname"
    },
    port: RegisteredPort
}

type DatabaseConnection {
    service: ServiceAddress,
    database: string?
}

type ConferDaemon {
    database: DatabaseConnection,
    service: ServiceAddress,
    auth: bool
}

type ConferProxy {
    service: ServiceAddress,
    hostPattern: HostPattern,
    defaultTTLMs: int? {
        default 60000,
        minimum 1
    }
}

enum AuthType {
    None,
    Symmetric,
    Asymmetric
}

schema hstefan.Confer {
    properties {
        conferDaemon: ConferDaemon,
        conferProxy: ConferProxy?
    }
    constraints {
        unique [conferDaemon.service, conferDaemon.database.service]
    }
}
"#;

        let res = FileDescriptor {
            enums: vec![
                EnumDefinition{
                    name: Identifier("AuthType"),
                    values: vec![Identifier("None"), Identifier("Symmetric"), Identifier("Asymmetric")]
                }
            ],
            constraints: vec![
                ConstraintDefinition {
                    name: Identifier("RegisteredPort"),
                    constrained_type: BaseType::Integer,
                    properties: vec![
                        ConstraintProperty {key: Identifier("minimum"), value: Literal::Integer(1024)},
                        ConstraintProperty {key: Identifier("maximum"), value: Literal::Integer(49151)}
                    ]
                },
                ConstraintDefinition {
                    name: Identifier("HostPattern"),
                    constrained_type: BaseType::Str,
                    properties: vec![
                        ConstraintProperty {key: Identifier("format "), value: Literal::Str("regex".to_owned())},
                    ]
                }
            ],
            types: vec![
                TypeDefinition {
                    name: Identifier("ServiceAddress"),
                    properties: vec![
                        PropertyType {
                            name: Identifier("hostname"),
                            type_name: Identifier("string"),
                            nullable: false,
                            constraints: Some(vec![
                                ConstraintProperty {
                                    key: Identifier("format"),
                                    value: Literal::Str("regex".to_owned())
                                }
                            ])
                        },
                        PropertyType {
                            name: Identifier("port"),
                            type_name: Identifier("RegisteredPort"),
                            nullable: false,
                            constraints: None
                        }
                    ]
                },
                TypeDefinition {
                    name: Identifier("DatabaseConnection"),
                    properties: vec![
                        PropertyType {
                            name: Identifier("service"),
                            type_name: Identifier("ServiceAddress"),
                            nullable: false,
                            constraints: None
                        },
                        PropertyType {
                            name: Identifier("database"),
                            type_name: Identifier("string"),
                            nullable: true,
                            constraints: None
                        }
                    ]
                },
                TypeDefinition {
                    name: Identifier("ConferDaemon"),
                    properties: vec![
                        PropertyType {
                            name: Identifier("database"),
                            type_name: Identifier("DatabaseConnection"),
                            nullable: false,
                            constraints: None
                        },
                        PropertyType {
                            name: Identifier("service"),
                            type_name: Identifier("ServiceAddress"),
                            nullable: true,
                            constraints: None
                        },
                        PropertyType {
                            name: Identifier("auth"),
                            type_name: Identifier("bool"),
                            nullable: false,
                            constraints: None
                        },
                    ]
                },
                TypeDefinition {
                    name: Identifier("ConferProxy"),
                    properties: vec![
                        PropertyType {
                            name: Identifier("service"),
                            type_name: Identifier("ServiceAddress"),
                            nullable: true,
                            constraints: None
                        },
                        PropertyType {
                            name: Identifier("hostPattern"),
                            type_name: Identifier("Hostpattern"),
                            nullable: false,
                            constraints: None
                        },
                        PropertyType {
                            name: Identifier("defaultTTLMs"),
                            type_name: Identifier("int"),
                            nullable: true,
                            constraints: Some(vec![
                                ConstraintProperty {
                                    key: Identifier("default"),
                                    value: Literal::Integer(60000)
                                },
                                ConstraintProperty {
                                    key: Identifier("minimum"),
                                    value: Literal::Integer(1)
                                }
                            ])
                        },
                    ]
                },
            ],
            schemas: vec![
                SchemaDefinition {
                    name: PathIdentifier(vec![Identifier("hstefan"), Identifier("Confer")]),
                    properties: vec![
                        PropertyType {
                            name: Identifier("conferDaemon"),
                            type_name: Identifier("ConferDaemon"),
                            nullable: false,
                            constraints: None,
                        },
                        PropertyType {
                            name: Identifier("conferProxy"),
                            type_name: Identifier("ConferProxy"),
                            nullable: true,
                            constraints: None,
                        },
                    ],
                    constraints: vec![SchemaConstraint {
                        key: Identifier("unique"),
                        properties: vec![
                            PathIdentifier(vec![Identifier("conferDaemon"), Identifier("service")]),
                            PathIdentifier(vec![Identifier("conferProxy"), Identifier("service")]),
                        ],
                    }],
                }
            ]
        };

        println!("{:?}", file_descriptor(src));
        assert_eq!(file_descriptor(src).unwrap().1, res);
    }
}
