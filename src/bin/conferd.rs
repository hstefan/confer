#![feature(proc_macro_hygiene, decl_macro)]

use confer::db::models::UserSchema;
use confer::db::schema::user_schemas;
use confer::json_schema::JsonSchema;
use diesel::prelude::*;
use env_logger;
use log::info;
use rocket::{get, put, routes};
use rocket_contrib::database;
use rocket_contrib::databases::diesel;
use rocket_contrib::json::Json;

#[database("confer")]
struct ConferDbConn(diesel::PgConnection);

#[get("/schemas/<name>/<version>")]
fn get_schema(conn: ConferDbConn, name: String, version: i32) -> Option<Json<JsonSchema>> {
    let schema: Option<UserSchema> = user_schemas::table
        .filter(
            user_schemas::name
                .eq(&name)
                .and(user_schemas::version.eq(version)),
        ).limit(1)
        .first::<UserSchema>(&conn.0)
        .optional()
        .expect("Couldn't find schema");

    if let Some(schema) = schema {
        info!("Found schema: {}:{}", schema.name, schema.version);
        None
    } else {
        None
    }
}

#[put("/schemas/<name>")]
fn put_schema(conn: ConferDbConn, name: String) -> String {
    format!("Hello, {}!", name)
}

fn main() {
    env_logger::init();
    info!("starting conferd");
    rocket::ignite()
        .attach(ConferDbConn::fairing())
        .mount("/", routes![get_schema, put_schema])
        .launch();
}
