CREATE TABLE user_schemas(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    version SERIAL NOT NULL,
    specification JSONB NOT NULL,
    UNIQUE (name, version)
);
