constraint RegisteredPort for Int {
    minimum 1024, maximum 49151
}

constraint HostPattern for String {
    format ["regex"]
}

type ServiceAddress {
    hostname: String {
        format ["hostname", "ipv4", "ipv6"]
    }
    port: RegisteredPort
}

type DatabaseConnection {
    service: ServiceAddress
    database: String?
}

type ConferDaemon {
    database: DatabaseConnection,
    service: ServiceAddress,
    auth: boolean? true
}

type ConferProxy {
    service: ServiceAddress,
    hostPatterns: [HostPattern],
    // inline generate a "constraint MangledName for u32"
    defaultTTLMs: u32? {
        default 60000,
        minimum 1
    },
}

schema hstefan.Confer {
    properties {
        conferDaemon: ConferDaemon,
        conferProxy: ConferProxy?
    }
    constraints {
        unique [conferDaemon.service, conferDaemon.database.service]
    }
}
